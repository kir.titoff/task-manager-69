package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.service.TaskService;
import ru.t1.ktitov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public void create() {
        taskService.create(UserUtil.getUserId(), "New Task " + System.currentTimeMillis());
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.update(UserUtil.getUserId(), task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskService.removeById(UserUtil.getUserId(), task.getId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }

}
