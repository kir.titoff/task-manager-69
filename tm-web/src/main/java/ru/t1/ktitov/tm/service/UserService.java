package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.model.User;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Transactional
    public User add(@Nullable final User model) {
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Transactional
    public Collection<User> add(@Nullable final Collection<User> models) {
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Transactional
    public User update(@Nullable final User model) {
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    public List<User> findAll() {
        return repository.findAll();
    }

    @Nullable
    public User findOneById(@Nullable final String id) {
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Transactional
    public User remove(@Nullable final User model) {
        repository.delete(model);
        return model;
    }

    @NotNull
    @Transactional
    public User removeById(@Nullable final String id) {
        Optional<User> model = repository.findById(id);
        repository.deleteById(id);
        return model.get();
    }

}
