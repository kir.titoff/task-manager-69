package ru.t1.ktitov.tm.dto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Result {

    private boolean success = true;

    private Exception exception;

    public Result(final boolean result) {
        this.success = result;
    }

    public Result(final Exception exception) {
        this.success = false;
        this.exception = exception;
    }

}
