package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Transactional
    public Task add(@Nullable final String userId, @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Transactional
    public Collection<Task> add(@Nullable final String userId, @Nullable final Collection<Task> models) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (models == null) throw new EntityNotFoundException();
        models.forEach(m -> m.setUserId(userId));
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Transactional
    public Task update(@Nullable final String userId, @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null || !userId.equals(model.getUserId())) throw new EntityNotFoundException();
        model.setUserId(userId);
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        return repository.findByUserId(userId);
    }

    @Nullable
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return repository.findByUserIdAndId(userId, id);
    }

    @NotNull
    @Transactional
    public Task remove(@Nullable final String userId, @Nullable final Task model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null || !userId.equals(model.getUserId())) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Transactional
    public Task removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        Optional<Task> model = repository.findById(id);
        repository.deleteById(id);
        return model.get();
    }

    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (name == null || name.isEmpty()) throw new EntityNotFoundException();
        @NotNull final Task task = new Task();
        task.setName(name);
        add(userId, task);
        return task;
    }

}
