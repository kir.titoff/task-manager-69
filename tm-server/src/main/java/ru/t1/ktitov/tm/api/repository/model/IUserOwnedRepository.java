package ru.t1.ktitov.tm.api.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.AbstractUserOwnedModel;

@Repository
public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {
}
