package ru.t1.ktitov.tm.logger;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
