package ru.t1.ktitov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.ISessionDtoService;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.UserNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO>
        implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

    @NotNull
    @Override
    public SessionDTO add(@Nullable final SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public Collection<SessionDTO> add(@Nullable final Collection<SessionDTO> models) {
        if (models == null) throw new EntityNotFoundException();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public SessionDTO update(@Nullable final SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<SessionDTO> set(@NotNull final Collection<SessionDTO> models) {
        clear();
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findByIdAndUserId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.findById(id).isPresent();
    }

    @Override
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    public SessionDTO remove(@Nullable final SessionDTO model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Override
    public SessionDTO removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        Optional<SessionDTO> model = repository.findById(id);
        if (!model.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @NotNull
    @Override
    public SessionDTO removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final SessionDTO model = repository.findByIdAndUserId(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null) throw new EmptyUserIdException();
        @Nullable final List<SessionDTO> projects = findAll(userId);
        if (projects == null) throw new ProjectNotFoundException();
        repository.deleteAll(projects);
    }

}
